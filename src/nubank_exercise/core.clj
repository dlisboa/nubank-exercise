(ns nubank-exercise.core
  (:require [nubank-exercise.models :as models]
            [nubank-exercise.util :as util]
            [clojure.math.numeric-tower :as num]))

(defn current-balance
  []
  (models/get-current-balance))

(defn- update-balance
  [operator amount timestamp]
  (let [balance (current-balance)
        new-balance (operator balance amount)]
    (models/save-balance new-balance timestamp)))

(defn credit
  ([amount] (credit amount (util/now)))
  ([amount timestamp] (update-balance + amount timestamp)))

(defn debit
  ([amount] (debit amount (util/now)))
  ([amount timestamp] (update-balance - amount timestamp)))

(defn perform-operation
  [op]
  (condp = (:type op)
    "credit" (credit (:amount op) (get op :timestamp (util/now)))
    "debit" (debit (:amount op) (get op :timestamp (util/now))))
  (models/save-operation op))

(defn statement
  [start end]
  (merge-with (fn [a b]
                {:operations a
                 :balance (last b)})
              (models/operations-by-date start end)
              (models/balances-by-date start end)))

(defn periods-of-debt
  [start end]
  (map
   (fn [balance]
     (let [start (:timestamp balance)]
       {:start start
        :end (util/day-before (models/next-balance balance))
        :principal (num/abs (:amount balance))}))
   (models/negative-balances-in-period start end)))
