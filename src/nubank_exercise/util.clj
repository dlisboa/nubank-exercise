(ns nubank-exercise.util
  (:require [clj-time.core :as t]
            [clj-time.coerce :as tc]
            [clj-time.format :as tf]))

(defn now [] (tc/to-sql-time (t/now)))

(defn format-timestamp
  [timestamp]
  (tf/unparse (tf/formatter "yyyy-MM-dd")
              (tc/from-sql-time timestamp)))

(defn day-before
  [balance]
  (when balance
    (tc/to-date
     (t/minus
      (tc/from-sql-time (:timestamp balance))
      (t/days 1)))))
