(ns nubank-exercise.web
  (:require [compojure.core :refer :all]
            [compojure.handler :as handler]
            [ring.middleware.json :refer :all]
            [clojure.walk :as w]
            [nubank-exercise.core :as core]
            [nubank-exercise.models :as models]))

(defn create-operation
  [req]
  (let [op (core/perform-operation (w/keywordize-keys (:body req)))]
    {:status 201
     :body op}))

(defn get-balance
  [_]
  {:status 200
   :body {:amount (core/current-balance)}})

(defn get-statement
  [{{:keys [start end]} :params}]
  (let [stmt (core/statement start end)]
    {:status 200
     :body stmt}))

(defn get-debt-periods
  [{{:keys [start end]} :params}]
  (let [periods (core/periods-of-debt start end)]
    {:status 200
     :body periods}))

(defroutes app-routes
  (POST "/operations" [] create-operation)
  (GET "/balance" [] get-balance)
  (GET "/statement" [] get-statement)
  (GET "/debt-periods" [] get-debt-periods))

(def app
  (-> app-routes
      handler/api
      wrap-json-response
      wrap-json-body))
