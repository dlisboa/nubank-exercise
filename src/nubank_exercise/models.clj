(ns nubank-exercise.models
  (:require [korma.db :as kdb]
            [korma.core :as k]
            [clj-time.coerce :as tc]
            [nubank-exercise.util :as util]))

(kdb/defdb db "postgresql://localhost:5432/nubank-exercise_test")

(k/defentity balances)
(k/defentity operations)


;; helper functions
(defn- select-in-period
  [entity start end]
  (let [[start end] (map tc/to-sql-time [start end])]
     (k/select entity
               (k/order :timestamp :asc)
               (k/where {:timestamp [between [start end]]}))))

(defn- group-by-date
  [rows]
  (group-by
   (comp util/format-timestamp :timestamp)
   rows))


;; balance
(defn save-balance
  ([amount] (save-balance amount (util/now)))
  ([amount timestamp]
   (let [sql-timestamp (tc/to-sql-time timestamp)]
    (k/insert balances
              (k/values [{:amount amount
                          :timestamp sql-timestamp}])))))

(defn all-balances
  []
  (k/select balances))

(defn get-current-balance
  []
  (get (first
        (k/select balances
                  (k/fields :amount)
                  (k/order :timestamp :desc)
                  (k/limit 1)))
       :amount
       0))

(defn balances-by-date
  [start end]
  (group-by-date (select-in-period balances start end)))

(defn delete-all-balances
  []
  (k/delete balances))

(defn negative-balances-in-period
  [start end]
  (filter
   (comp neg? :amount)
   (select-in-period balances start end)))

(defn next-balance
  [balance]
  (first
   (k/select balances
             (k/limit 1)
             (k/order :timestamp :asc)
             (k/where {:timestamp [> (:timestamp balance)]}))))


;; operation
(defn save-operation
  [operation]
  (let [op (merge {:timestamp (util/now)} operation)]
    (k/insert operations
              (k/values [(update op :timestamp tc/to-sql-time)]))))

(defn delete-all-operations
  []
  (k/delete operations))

(defn operations-by-date
  [start end]
  (group-by-date (select-in-period operations start end)))

(defn all-operations
  []
  (k/select operations))
