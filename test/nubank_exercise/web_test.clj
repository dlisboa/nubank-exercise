(ns nubank-exercise.web-test
  (:require  [clojure.test :refer :all]
             [clojure.data.json :as json]
             [ring.mock.request :refer [request body content-type]]
             [nubank-exercise.models :as models]
             [nubank-exercise.models-test :refer [clean-database]]
             [nubank-exercise.web :refer :all]))

(use-fixtures :each clean-database)

(deftest balance-test
  (testing "returns a JSON response with the current balance"
    (let [balance (models/save-balance 1234.56M)
          response (app (request :get "/balance"))]
      (is (= 200 (:status response)))
      (is (= "{\"amount\":1234.56}" (:body response))))))

(defn- post-operation
  [op]
  (app (-> (request :post "/operations")
           (body (json/write-str op))
           (content-type "application/json"))))

(deftest create-operation-test
  (testing "create an operation based on a JSON body"
    (let [response (post-operation {:type "debit" :amount 180.00M})]
      (is (= 1 (count (models/all-operations))))
      (is (= 201 (:status response))))))

(deftest statement-test
  (testing "returns the statement for a given period as JSON"
    (post-operation {:type "credit" :amount 1000.00M :timestamp "2010-10-15"})
    (post-operation {:type "debit" :amount 200.00M :timestamp "2010-10-16"})

    (let [response (app (request :get "/statement?start=2010-10-01&end=2010-10-30"))
          json-body (json/read-str (:body response))]
      (is (= 200 (:status response)))
      (is (= 1 (count (get-in json-body ["2010-10-15" "operations"]))))
      (is (= 1000.00 (get-in json-body ["2010-10-15" "balance" "amount"])))
      (is (= 800.00 (get-in json-body ["2010-10-16" "balance" "amount"]))))))

(deftest debt-periods-test
  (testing "returns the periods of debt as JSON"
    (post-operation {:type "credit" :amount 1000.00M :timestamp "2010-10-15"})
    (post-operation {:type "debit" :amount 2000.00M :timestamp "2010-10-16"})
    (post-operation {:type "credit" :amount 1010.00M :timestamp "2010-10-19"})
    (post-operation {:type "debit" :amount 30.00M :timestamp "2010-10-21"})

    (let [response (app (request :get "/debt-periods?start=2010-10-01&end=2010-10-30"))
          json-body (json/read-str (:body response))]
      (is (= 200 (:status response)))
      (is (= 1000.00 (get (first json-body) "principal")))
      (is (= 20.00 (get (last json-body) "principal"))))))
