(ns nubank-exercise.core-test
  (:require [clojure.test :refer :all]
            [nubank-exercise.core :refer :all]
            [nubank-exercise.models :refer :all]
            [nubank-exercise.models-test :refer [clean-database]]))

(use-fixtures :each clean-database)

(deftest credit-saves-balance-test
  (testing "saves a new balance"
    (credit 200.00M)
    (is (= 200.00M (current-balance))))

  (testing "calculates the new balance using the last balance"
    (save-balance 1000.00M)
    (credit 200.00M)
    (is (= 1200.00M (current-balance)))))

(deftest debit-test
  (testing "saves a new balance"
    (debit 200.00M)
    (is (= -200.00M (current-balance))))

  (testing "calculates the new balance using the last balance"
    (save-balance 1000.00M)
    (debit 200.00M)
    (is (= 800.00M (current-balance)))))

(deftest perform-operation-saves-operation-test
  (let [op {:type "credit"
            :info "Deposit"
            :amount 1000.00M}]
    (testing "saves the operation"
      (perform-operation op)
      (is (= 1000.00M (:amount (first (all-operations))))))))

(deftest perform-credit-operation-test
  (let [op {:type "credit"
            :info "Deposit"
            :amount 1000.00M}]
    (testing "perform a credit operation"
      (perform-operation op)
      (is (= 1000.00M (current-balance))))))

(deftest perform-debit-operation-test
  (let [op {:type "debit"
            :info "Purchase at Amazon"
            :amount 3.34M}]
    (testing "perform a debit operation"
      (perform-operation op)
      (is (= -3.34M (current-balance))))))

(deftest statement-test
  (testing "returns a map with operations and balances indexed by date"
    (perform-operation {:type "credit"
                        :info "Deposit"
                        :amount 1000.0M
                        :timestamp "2010-04-01"})
    (perform-operation {:type "debit"
                        :info "Purchase on Amazon"
                        :amount 3.34M
                        :timestamp "2010-04-02T10:00"})
    (perform-operation {:type "debit"
                        :info "Purchase on Uber"
                        :amount 45.23M
                        :timestamp "2010-04-02T15:00"})

    (let [result (statement "2010-04-01" "2010-04-10")]
      (is (= 951.43M
             (get-in result ["2010-04-02" :balance :amount])))
      (is (= "Purchase on Uber"
             (:info (last (get-in result ["2010-04-02" :operations]))))))))

(deftest periods-of-debt-test
  (testing "returns a sequence of maps with the start and end of each debt period, as well as the amount owed"
    (credit 100.00M "2010-10-01")
    (debit 200.00M "2010-10-02")
    (credit 400.00M "2010-10-05")
    (debit 500.00M "2010-10-06")
    (debit 1000.00M "2010-10-08")
    (credit 2000.00M "2010-10-09")
    (debit 1000.00M "2010-10-11")

    (let [expected [{:start #inst"2010-10-02"
                     :end #inst"2010-10-04"
                     :principal 100.0M}
                    {:start #inst"2010-10-06"
                     :end #inst"2010-10-07"
                     :principal 200.0M}
                    {:start #inst"2010-10-08"
                     :end #inst"2010-10-08"
                     :principal 1200.0M}
                    {:start #inst"2010-10-11"
                     :end nil
                     :principal 200.0M}]]
      (is (= expected
             (periods-of-debt "2010-10-01" "2010-10-30"))))))
