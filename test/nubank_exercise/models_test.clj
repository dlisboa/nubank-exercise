(ns nubank-exercise.models-test
  (:require  [clojure.test :refer :all]
             [nubank-exercise.models :refer :all]
             [clj-time.coerce :as tc]
             [clj-time.core :as t]))

(defn clear-database
  []
  (delete-all-balances)
  (delete-all-operations))

(defn clean-database
  [tests]
  (clear-database)
  (try
    (tests)
    (finally
      (clear-database))))

(use-fixtures :each clean-database)

(deftest save-balance-test
  (testing "persists balance to database"
    (save-balance 1000.00M)
    (is (= 1 (count (all-balances))))))

(deftest save-balance-accepts-timestamp-test
  (testing "accepts a timestamp"
    (save-balance 1000.00M "2010-10-01")
    (is (= 2010
           (t/year (tc/from-sql-time (:timestamp (first (all-balances)))))))))

(deftest group-balances-by-date-test
  (testing "returns a map with balances indexed by date"
    (let [a (save-balance 1000.00M "2010-04-01")
          b (save-balance 800.00M "2010-04-02T10:00")
          c (save-balance 650.00M "2010-04-02T15:00")
          result {"2010-04-01" [a]
                  "2010-04-02" [b c]}]
      (is (= result (balances-by-date "2010-04-01"
                                      "2010-04-10"))))))

(deftest get-current-balance-test
  (testing "returns 0 if there are no balances"
    (is (= 0 (get-current-balance))))

  (testing "grabs the last balance based on timestamp"
    (save-balance 80.00M)
    (save-balance 120.00M)
    (is (= 120.00M (get-current-balance)))))

(deftest negative-balances-in-period-test
  (testing "returns only negative balances for a given period"
    (let [a (save-balance 100.00M "2010-10-01")
          b (save-balance -100.00M "2010-10-03")
          c (save-balance 50.00M "2010-10-04")]
      (is (every? #(= % b)
                  (negative-balances-in-period "2010-10-01"
                                               "2010-10-30"))))))

(deftest next-balance-text
  (testing "returns the next balance in cronological order"
    (let [a (save-balance 100.00M "2010-10-01")
          b (save-balance -100.00M "2010-10-03")
          c (save-balance 50.00M "2010-10-04")]
      (is (= b (next-balance a))))))

(deftest save-operation-test
  (testing "persists operation to database"
    (let [op {:type "credit"
              :info "Deposit"
              :amount 1000.00M}]
      (save-operation op)
      (is (= 1 (count (all-operations))))
      (is (= "Deposit" (:info (first (all-operations))))))))

(deftest save-operation-accepts-timestamp-test
  (testing "accepts a timestamp"
    (let [op {:type "debit"
              :info "Purchase at Amazon"
              :amount 3.34M
              :timestamp "2010-01-01"}]
      (save-operation op)
      (is (= 2010 (t/year (tc/from-sql-time
                           (:timestamp (first (all-operations))))))))))

(deftest group-operations-by-date-test
  (testing "returns a map with operations indexed by date"
    (let [op1 {:type "credit"
               :info "Deposit"
               :amount 1000.00M
               :timestamp "2010-04-01"}
          op2 {:type "debit"
               :info "Purchase on Amazon"
               :amount 3.34M
               :timestamp "2010-04-02T10:00"}
          op3 {:type "debit"
               :info "Purchase on Uber"
               :amount 45.23M
               :timestamp "2010-04-02T15:00"}
          a (save-operation op1)
          b (save-operation op2)
          c (save-operation op3)
          result {"2010-04-01" [a]
                  "2010-04-02" [b c]}]
      (is (= result (operations-by-date "2010-04-01"
                                        "2010-04-10"))))))
