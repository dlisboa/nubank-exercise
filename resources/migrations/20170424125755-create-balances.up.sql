CREATE TABLE balances (
  id integer,
  amount numeric(15,2),
  timestamp timestamp
);
--;;
CREATE SEQUENCE balances_id_seq;
--;;
ALTER TABLE balances ALTER COLUMN id SET DEFAULT nextval('balances_id_seq');
