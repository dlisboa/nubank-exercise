CREATE TABLE operations (
  id integer,
  amount numeric(15,2),
  info text,
  type text,
  timestamp timestamp
);
--;;
CREATE SEQUENCE operations_id_seq;
--;;
ALTER TABLE operations ALTER COLUMN id SET DEFAULT nextval('operations_id_seq');

