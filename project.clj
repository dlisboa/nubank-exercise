(defproject nubank-exercise "0.1.0-SNAPSHOT"
  :description "FIXME: write description"
  :url "http://example.com/FIXME"
  :license {:name "Eclipse Public License"
            :url "http://www.eclipse.org/legal/epl-v10.html"}

  :dependencies [[org.clojure/clojure "1.8.0"]
                 [org.postgresql/postgresql "42.0.0"]
                 [migratus "0.9.0"]
                 [clj-time "0.13.0"]
                 [korma "0.4.3"]
                 [ring/ring-core "1.5.1"]
                 [ring/ring-jetty-adapter "1.5.1"]
                 [ring/ring-mock "0.3.0"]
                 [ring/ring-json "0.4.0"]
                 [compojure "1.5.1"]
                 [org.clojure/data.json "0.2.6"]
                 [org.clojure/math.numeric-tower "0.0.4"]]

  :plugins [[migratus-lein "0.4.4"]
            [lein-ring "0.11.0"]]

  :migratus {:store :database
             :migration-dir "migrations"
             :db ~(get (System/getenv) "DATABASE_URL")}

  :ring {:handler nubank-exercise.web/app
         :nrepl {:start? true}}

  :profiles {:test {:env {:database-url "postgresql://localhost:5432/nubank-exercise_test"}}})
